const express = require("express");

const router = express.Router();
const mongoose = require("mongoose");
// şifreleri hashlemek için
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require('../models/user');

router.get('/all_users', (req, res, next) =>{
    User.find()
        .select("_id email") //göndermek istediğin fieldları seçiyorsun
        .exec()
        .then(docs=>{
            const response = { //yeni bir var yaratıp sayısını ve productaları gönderiyorsun
                count: docs.length,
                users: docs.map(doc=>{
                    return {
                        email: doc.email,
                        _id: doc._id,
                        request: {
                            type:"GET",
                            url:"http://localhost:3000/products/" + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch( err => {
            console.log(err);
            res.status(500).json({error : err});
        });
});

router.post('/login', (req, res, next) =>{
    User.find({email:req.body.email})  //find, bir array döndürürken findone tek bir örnek döndürür
        .exec()
        .then(user=>{
            //eğer böyle bir mail adresi yoksa
            if(user.length < 1){
                res.status(401).json({  //401: unauthorized
                    message:"Auth failed"
                });
            }else{
                const userId = user[0]._id;
                console.log(userId);
                bcrypt.compare(req.body.password, user[0].password,(err,result)=>{
                    //eğer şifreler uyuşmuyorsa
                    if(err){
                        return res.status(401).json({
                            message:"Auth failed"
                        });
                    }
                    if(result){
                        //şifreler uyuşuyorsa token oluşturuyor 2.si gizli kelime 3.sü ne kadar zaman geçerli
                        const token = jwt.sign({
                            email : user[0].email,
                            userId : user[0]._id
                        },
                            process.env.JWT_KEY,
                            {
                                expiresIn : "1h"
                            });
                        return res.status(200).json({
                            message : "Auth successful",
                            userId : userId,
                            token : token
                        });
                    }
                    res.status(401).json({
                        message:"Auth failed"
                    });
                });

            }

        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });

});

router.post('/signup', (req, res, next) => {
    User.find({email: req.body.email})
        .exec()
        .then(user=>{
            if(user.length >= 1 ){
                return res.status(409).json({ //409: conflict, 422: unprocessable entity
                    message : "This mail adress exists"
                });
            }
            else{
                bcrypt.hash(req.body.password,10,(err,hash)=>{
                    if(err){
                        res.status(500).json({
                            error:err
                        });
                    }
                    else{
                        const user = new User({
                            _id : new mongoose.Types.ObjectId(),
                            email : req.body.email,
                            password : hash
                        });
                        user
                            .save()
                            .then(result => {
                                console.log(result);
                                res.status(201).json({
                                    message : "User created"
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error:err
                                });
                            });

                    }
                });

            }
        });

});

router.delete("/:userId", (req, res, next) => {
    User.remove({_id: req.params.userId})
        .exec()
        .then(result => {
            res.status(200).json({
                message : "User deleted"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});


module.exports = router;